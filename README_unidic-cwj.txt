■README
現代書き言葉UniDic ver.2023.02 軽量版パッケージ
https://clrd.ninjal.ac.jp/unidic/
形態素解析器MeCab用
https://taku910.github.io/mecab/

■COPYRIGHT
国立国語研究所（言語資源開発センター/「語彙資源」プロジェクト）

■IMPLEMENTOR
小木曽智信 togiso@ninjal.ac.jp

■LICENSE
GPL v2.0/LGPL v2.1/修正BSD
licensesフォルダ内参照

■HISTORY
ver.2023.02
-パッケージ内容を簡素化しました。
-新たな語彙を追加しました。
-語彙素細分類の一部の末尾についていた空白文字を除去しました。
-学習素性を一部見直しました。

これ以前の記録は下記をご覧ください。
https://clrd.ninjal.ac.jp/unidic/back_number.html#unidic_bccwj

